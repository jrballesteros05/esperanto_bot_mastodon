# Mastodon bot Spanish-Esperanto

This is a simple bot which toots randomly in Mastodon and also answer some questions.

This script is inspired by "PetraOleum" work on https://github.com/PetraOleum/vortaro. Because it does not have license, see [here](https://choosealicense.com/no-permission/), I decided to not fork the project so I wrote this script from scratch. Other differences from original script:

- This bot toots words in Esperanto for Spanish instead of English.
- The dictionary is downloaded automatically from [here](https://aulex.org/es-eo/lexicon.php), so, in theory, you don't have to maintain a list of words.
- This bot is a bit more interactive, if you mention in the toot with some parameters it will try to answer with the translation.

_Despite the translation will be in Spanish I prefer to comment this code in English, mainly because It would be easier to read by everyone in the world and if someone is interested could fork it without problem and adapt it to any language._

# What inspired me?

I love Esperanto and I love libre software, I love both philosophies about a world where everybody is free and we can communicate without barriers.

# Usage

## Automate random usage

The bot I created is located [here](https://botsin.space/@esperantavortaro). This boot will toot randomly every hour.

## "Interactive" usage

You have to mention the bot and write the pattern "es:yourword" for spanish or "eo:yourword" for esperanto, for example if you toot:

     @esperantavortaro@botsin.space give me the meaning of es:hola

The boot will look for the word "hola" in its dictionary and it will reply you with something like this: 

    @yourname@instance El significado de "hola" es "saluton".

If the boot does not have the answer it will reply like this: 

    @yourname@instance No se de que me hablas humano. 

# Install you own bot. 


1. Create an account in any Mastodon instance, I personally suggest [botsin](https://botsin.space/) mainly because is a instance for bots, if you want to create in other instance please read the policy about that instance. 

2. Go to Settings-\>Development and create a new application, give it a name, set the permissions you want and submit.
 
3. Clone this repository

        git clone https://codeberg.org/jrballesteros05/esperanto_bot_mastodon.git

4. Go back to the mastodon website and go to Settings-\>Development-\>Your Applications and copy the "access\_token".
5. Edit the file "Configs.ini" in the section [MASTODON] and fill the access\_token you copied before. Also put the URL of the site, if you create the account in https://botsin.space/ yous put that as URL. 

6. Feel free to modify other parameters like [CHECKPOINT]. I do not suggest you to modify [DEFAULT] at least you know what you are doing. 

7. Run the script

        python dictionary_es_eo.py

   _Note: Be sure you installed Mastodon.py and Beautifoul soup libraries_

8. Set up a cron to run automatically the time you want. Once again, be careful about the mastodon instance policy, do not abuse. I suggest you to run the bot every hour. 

# Brief explanation how the script works

1. The script downloads the dictionary from [here](https://aulex.org/es-eo/lexicon.php).
2. It parse the html with beautiful soup library and create a python dictionary where the key is the word in spanish and the value is the word in esperanto. 
3. Then connect to Mastodon.
4. Check if there are mentions and it will reply every single of them. 
5. Toot a random word. 
 
# Limitations

- The bot is so dumb so do not be dumber than the bot. Check you spelling before asking something.
- This bot does not check anything before, so if you feel there are error, please report. 
- If you want just the "interactive" mode you have to comment the last line in the code. In this way you can run the cron every 5 minutes and get replies faster. But please do not flood the instance with toots. 
