#!/usr/bin/env python
# coding: utf-8

from requests import get
from mastodon import Mastodon
from zipfile import ZipFile
from bs4 import BeautifulSoup
from pathlib import Path
import configparser
import re
import random


"""
This script is inspired by "PetraOleum" work on https://github.com/PetraOleum/vortaro. Because it does not have
license I decided to not copy anything. Other differences from original script:

- This bot toots words in Esperanto for Spanish instead of English. 
- The dictionary is downloaded automatically from https://aulex.org/es-eo/lexicon.php, so, in theory, you don't 
    have to maintain a list of words.
- This bot is a bit more interactive, if you mention in the toot with some parameters it will try to 
  answer with the translation. 
  
Despite the translation will be in Spanish I prefer to comment this code in English, mainly because It would be
easier to read by everyone in the world. 

----------------------------------------------------------------------------------------------------------------
Este script está inspirado en el trabajo de "PetraOleum" https://github.com/PetraOleum/vortaro. Como el trabajo
original no estaba licenciado he decidido no copiar nada de su código. Otras diferencias con el script original:

- Este bot traduce palabras al esperanto del Español en lugar del Inglés.
- El diccionario se descarga automáticamente de https://aulex.org/es-eo/lexicon.php. así que, en teoría,
  no es necesario mantener una lista de palabras.
- Este bot es poco mas interactivo, si lo mencionas en un toot con ciertos parámetros el tratará de 
  responderte el significado. 
  
Si bien las traducciones serán en Español, prefiero comentar el código en Inglés, principalmente para que la lectura
sea mas sencilla para todo el mundo.
"""

def get_config_variables(conf_file="Configs.ini"):
    """
    Function that reads config values from .ini config file and parse it
    Parameters:
    ----------
    conf_file : Configuration file, by default "Configs.ini"
    Returns:
    -------
    Values parsed from .ini
    """
    config = configparser.ConfigParser()
    config.sections()
    config.read(conf_file)
    return(config)

def download_vocabulary():
    """
    Function that downloads vocabulary from page https://aulex.org/es-eo/lexicon.php and store
    the file inside the hard disk
    """
    url_vocabulary = get_config_variables()['DEFAULT']['URL_VOC']
    #zip_file = "vocabulary.zip"
    response = get(url_vocabulary)
    open("es-eo.txt", "wb").write(response.content)

def parse_html():
    """
    This function recibe a vocabulary soup as parameter, we filter all the things we don't need it
    Parameters:
    ----------
    vocabulary_soup : beautiful soup with vocabulary in Spanish and Esperanto.
    Returns:
    -------
    sp_dictionary: Simple dictionary with key Spanish word and Value the Esperanto meaning.
    """
    sp_dictionary = {}
    vocabulary = open("es-eo.txt", "r")
    for i in vocabulary:
        phrase = i.split(':')
        if len(phrase) == 2:
            es = phrase[0].rstrip()
            eo = phrase[1].lstrip()
            sp_dictionary[es] = eo
    return sp_dictionary

def connect_to_mastodon():
    """
    Reads the config file access_token and url and connect to mastodon instance.
    
    Returns:
    -------
    A mastodon object.
    """
    mastodon = Mastodon(
    access_token = get_config_variables()['MASTODON']['ACCESS_TOKEN'],
    api_base_url = get_config_variables()['MASTODON']['URL']
    )
    return(mastodon)

def interactive_translate(toot_notification_content,vocabulary_dict):
    """
    This function receives a content, match with regex, if matchs look for the dictionary and return the value
    if exists. If does not match it just return a message that say "I do not what are you talking about Human"
    
    Parameters:
    ----------
    toot_notification_content : beautiful soup with vocabulary in Spanish and Esperanto.
    vocabulary_dict: Dictionary of words.
    
    Returns:
    -------
    A simple string in Spanish or Esperanto with the meaning of the words or just 
    "I do not what are you talking about Human"
    """
    content_soup = BeautifulSoup(toot_notification_content.status.content)
    raw_content = content_soup.p.text
    raw_content = raw_content.lower()
    username = toot_notification_content.status.account.acct
    sp_regex = re.compile("(ES|es|Es)\:.*?(?P<spanish_word>.*)")
    eo_regex = re.compile("(EO|eo|Eo)\:.*?(?P<esperanto_word>.*)")
    ask_spanish = sp_regex.search(raw_content)
    ask_esperanto = eo_regex.search(raw_content)
    if ask_spanish:
        sp_word = ask_spanish.group("spanish_word").replace(".","")
        try:
            eo_meaning = vocabulary_dict[sp_word]
            return("@%s El significado de \"%s\" es \"%s\"." % (username,sp_word,eo_meaning))
        except:
            return("@%s No se de que me hablas humano." % (username))
    if ask_esperanto:
        eo_word = ask_esperanto.group("esperanto_word").replace(".","")
        try:
            sp_meaning = list(vocabulary_dict.keys())[list(vocabulary_dict.values()).index(eo_word)]
            return("@%s La signifo de \"%s\" estas \"%s\"." % (username,eo_word,sp_meaning))
        except:
            return("@%s Mi ne scias, kion vi parolas pri homo." % (username))
    else:
        return("@%s No se de que me hablas humano." % (username))

def get_checkpoint(toot_notification_content):
    """
    Reads the file from Configs.ini and returns the notifications from that id. If the file does not exists
    or for any reason it losts the function will get the last id from notifications and start the checkpoint
    from it. Mainly because I'd rather the bot don't answer some toots instead of reading the toots from the 
    beggining and messing the instance with repeated toots. 
    Returns:
    -------
    An integer with the last mention ID.
    """
    checkpoint_file = get_config_variables()['CHECKPOINT']['CHECKPOINT_FILE']
    checkpoint_file_path = Path(checkpoint_file)
    if checkpoint_file_path.is_file() :
        #File already exist, reading checkpoint from there..
        if checkpoint_file_path.stat().st_size == 0:
            #"File exist but is empty, so getting last mention toot...
            checkpoint = toot_notification_content[0].id
            with open(checkpoint_file, "w") as cf:
                cf.write(str(checkpoint))
        else:
            with open(checkpoint_file, "r") as cf:
                checkpoint = cf.read()
    else:
        #File does not exist, so getting last mention toot...
        checkpoint = toot_notification_content[0].id
        with open(checkpoint_file, "w") as cf:
            cf.write(str(checkpoint))
    return (int(checkpoint))

def write_last_checkpoint(last_id):
    """Just writes the checkpoint file with an ID number"""
    checkpoint_file = get_config_variables()['CHECKPOINT']['CHECKPOINT_FILE']
    with open(checkpoint_file, "w") as cf:
        cf.write(str(last_id))

def answer_mentions(mastodon_object, checkpoint, vocabulary):
    """Read the notifications since the checkpoint ID and answer one by one
    Parameters:
    ----------
    mastodon_object : mastodon object
    checkpoint: The last mention id.
    vocabulary: Dictionary with the words in Spanish and Esperanto
    """
    id_flag = False
    for n in mastodon_object.notifications(since_id=checkpoint):
        if n.type == "mention":
            #Mention notification, processing
            translation = interactive_translate(n, vocabulary)
            mastodon_object.status_post(translation,in_reply_to_id=n.status)
            last_id = n.id
            id_flag = True
    if id_flag:
        print("Oh, new mentions, updating checkpoint...")
        write_last_checkpoint(last_id)
            
def random_toot(mastodon_object, vocabulary):
    """Just take any word from the dictionary and toot it randomly
    Parameters:
    ----------
    mastodon_object : mastodon object
    vocabulary: Dictionary with the words in Spanish and Esperanto
    """
    toot_formats = [ "Español: \'{}.\'\nEsperanto: \'{}\'",
                "En lugar de decir \'{}\', di \'{}\'",
                "La palabra en Esperanto para \'{}\' es \'{}\'",
                "Intenta utilizar \'{}\' para \'{}\'",
                "{} = {}",
                "Para decir \'{}\' puedes usar \'{}\'",
                "Si te gusta el Esperanto, intenta decir \'{1}\' para \'{0}\'",
                "Esperanto: \'{1}\'\nEspañol: \'{0}.\'"
                   ]
    formats_len = len(toot_formats)
    random_format_int = random.randint(0, formats_len - 1)
    random_format = toot_formats[random_format_int]
    sp, eo = random.choice(list(vocabulary.items()))
    random_toot = random_format.format(sp,eo)
    mastodon_object.status_post(random_toot)    

if __name__ == '__main__':
    """The main function, donwload the vocabulary from net, put into a dictionary, then answer the
    mentions and finally toot randomly.
    """
    my_vocabulary = download_vocabulary()
    vocabulary_dictionary = parse_html()
    mastodon = connect_to_mastodon()
    checkpoint = get_checkpoint(mastodon.notifications())
    answer_mentions(mastodon, checkpoint, vocabulary_dictionary)
    random_toot(mastodon,vocabulary_dictionary)

